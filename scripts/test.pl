#!/usr/bin/perl
use warnings;
use strict;

print << 'EOL';

Execution of test completed
************************************************************
	start_time           = Mon Jul 31 01:25:00 EDT 2017
	end_time             = Mon Jul 31 01:29:37 EDT 2017
	plan_name            = sumpande-fmc_ha_dev
	plan_status          = completed
	docker_image         = pyats-3.3
	pending              = 0
	running              = 0
	cancelled            = 0
	failed               = 0
	passed               = 1
	total                = 1
************************************************************
Test result details: 
Number of test cases = 1
============================================================
	test case sequence   = 1
	test case id         = 597ebfbd98c69724b8180b05
	test case name       = DeployAllOVF
	test case state      = failed
	test case start time = 1501478734
	test case end time   = 1501478974
============================================================

EOL
